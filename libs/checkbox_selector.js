/*
 * Author: Anthony Nwokafor
 * email: anthony.nwokafor@gmail.com
 */
var CheckBoxSelector = (function ($) {
    var _defaults = {
        onCheckAll: null,
        onCheckSingle: null,
    };

    var _onCheckAll = function() {
        checked = this.all.prop('checked');
        this.checked = [];
        if (checked) {
            this.checked = this.singles.map(function() {
                return $(this).attr("id"); 
            }).get();
        }
        
        this.singles.prop('checked', checked);
        console.log(this.checked);
        if (this.options.onCheckAll) {
            this.options.onCheckAll(this.singles, checked);
        }
    };

    var _onCheckSingle = function(elem) {
        target = $(elem.target);
        id = target.attr("id");
        checked = target.prop('checked');
        index = this.checked.indexOf(id);
        if (checked && index == -1) {
            this.checked.push(id);
        } else if (!checked && index >= 0) {
            this.checked.splice(index, 1);
        }
        console.log(this.checked);
        if (this.checked.length == this.singlesCount) {
            this.singles.prop('checked', true);
            this.all.prop('checked', true);
        } else {
            this.all.prop('checked', false);
        }

        if (this.options.onCheckAll) {
            this.options.onCheckAll(this.singles, this.checked.length);
        }
        
        if (this.options.onCheckSingle) {
            this.options.onCheckSingle(target, checked);
        }
    };
    
    var CheckBoxSelector = function(all, singles, options) {
        this.all = $(all);
        this.singles = $(singles);
        this.options = $.extend({}, _defaults, options || {});
        this.singlesCount = this.singles.length;
        this.checked = [];
        this.all.click(_onCheckAll.bind(this));
        this.singles.click(_onCheckSingle.bind(this));
    };

    CheckBoxSelector.prototype = function() {
        return {
            selected: this.checked
        }
    } ();

    return CheckBoxSelector;
}) (jQuery);

